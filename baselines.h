/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <vector>
#include "matrix.cc"
using namespace std;

#define NUM_STANDS 256

class Baseline {
public:
  int st1, st2;
  bool has_data;
  JonesMatrix<float> vis, model;
  
  Baseline() { st1 = st2 = 0; has_data = false; }
  Baseline(int s1, int s2, float values[16]);
  Baseline(const Baseline& bl);
  Baseline& operator=(const Baseline& bl);


  void print() { cout << "BL " << st1 << " "  << st2 << endl; vis.print(); model.print(); }
};

class Baselines {
public:
  int num_baselines;
  Baseline *baselines;
  JonesMatrix<float> *init_jones;
  vector<int> stands_present;
  int location[NUM_STANDS][NUM_STANDS];

  Baselines() { baselines = NULL; for (int i=0; i<NUM_STANDS; ++i) for (int j=0; j<NUM_STANDS; ++j) location[i][j] = -1; }
  Baselines(const char *fname);
  ~Baselines() { if ( baselines != NULL ) delete[] baselines; if ( init_jones != NULL ) delete[] init_jones; }

  int find(int st1, int st2);
  bool in(int val) { for (int i=0; i<stands_present.size(); ++i) if ( stands_present[i] == val ) return true; return false; }
};
