/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <cstdlib>
#include "optimize.h"
#include "ceres/ceres.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;


Optimize::Optimize(StandMapping& stands_mapping, Baselines& baselines, JonesMatrix<float> *J_cur) {
	stands = stands_mapping.mapping;
	levmar_result = -1;
	
	J = new JonesMatrix<float>[stands.size()];
	if ( J == NULL ) {
          cerr  << "Failed to allocate J in Optimize\n";
          exit(1);
	}
	V = new JonesMatrix<float>*[stands.size()];
        if ( V == NULL ) {
          cerr  << "Failed to allocate V in Optimize\n";
          exit(1);
        }
	for (int i=0; i<stands.size(); ++i) {
	  V[i] = new JonesMatrix<float>[stands.size()];
          if ( V[i] == NULL ) {
            cerr  << "Failed to allocate V[i] in Optimize\n";
            exit(1);
          }
        }
	P = new JonesMatrix<float>*[stands.size()];
        if ( P == NULL ) {
          cerr  << "Failed to allocate P in Optimize\n";
          exit(1);
        }
	for (int i=0; i<stands.size(); ++i) {
	  P[i] = new JonesMatrix<float>[stands.size()];
          if ( P[i] == NULL ) {
            cerr  << "Failed to allocate P[i] in Optimize\n";
            exit(1);
          }
        }
	for (int i=0; i<stands.size(); ++i) {
		int st1 = stands[i]; 
		J[i] = J_cur[st1]; 
		for (int j=i+1; j<stands.size(); ++j) {
			int st2 = stands[j];
			if ( st2 == -1 ) {
				cerr << "Consistency err: asked to map invalid stand in Optimize\n";
				exit(1);
			}
			int bl_index = baselines.find(st1, st2);
			if ( bl_index != -1 ) {
       

				V[i][j] = baselines.baselines[bl_index].vis;     
				P[i][j] = baselines.baselines[bl_index].model;
				
        
			} else {
				V[i][j].in_use = false;
				P[i][j].in_use = false;
			}
		} 
	}

}    

Optimize::~Optimize() {
  delete[] J;
  
  for (int i=0; i<stands.size(); ++i) { delete[] V[i]; delete[] P[i]; }
  delete[] V; delete[] P;
 
}


float Optimize::residual() {  // Between V and J.P.Jt
  double res = 0;
  float V_sum=0, V_residual=0; 
  
  for (int j=0; j<stands.size(); ++j) {
    for (int k=j+1; k<stands.size(); ++k) {    // Not using the whole thing
    	//if ( squared_norm(V[j][k]) == 0 ) cout << " x ";
    	//else cout << " . ";
    	if ( V[j][k].in_use  ) {
    		V_sum += V[j][k].norm();
    
    		//cout << "----" << j << " " << k << endl;
    		//V[j][k].print(); J[j].print(); J[k].print(); P[j][k].print(); (J[j]*P[j][k]*J[k].conjugate_transpose()).print(); 
    		V_residual += (V[j][k]-(J[j]*P[j][k]*J[k].conjugate_transpose())).norm();
    	}
    }
    //cout << endl;
  }
  return  V_residual/V_sum;  
}

struct BaselineResidual {

  BaselineResidual(JonesMatrix<float> V_, JonesMatrix<float> P_, int st1_, int st2_) 
      : V(V_), P(P_), st1(st1_), st2(st2_) {
	  
	  //cout << st1 << " " << st2 << endl;
	  //V.print(); P.print();
  }

  template <typename T> 
  bool operator()(const T* const jones_vals, T* residual) const {
	  float flattened_f[8];
	  T flattened_T[8];

	JonesMatrix<T> Jj, Jk; 
	Jj.set(complex<T>(jones_vals[st1*8+0], jones_vals[st1*8+1]), 
			complex<T>(jones_vals[st1*8+2], jones_vals[st1*8+3]), 
			complex<T>(jones_vals[st1*8+4], jones_vals[st1*8+5]), 
			complex<T>(jones_vals[st1*8+6], jones_vals[st1*8+7]));
	Jk.set(complex<T>(jones_vals[st2*8+0], jones_vals[st2*8+1]), 
		complex<T>(jones_vals[st2*8+2], jones_vals[st2*8+3]), 
		complex<T>(jones_vals[st2*8+4], jones_vals[st2*8+5]), 
		complex<T>(jones_vals[st2*8+6], jones_vals[st2*8+7]));
        
	JonesMatrix<T> V_residual, Pt;
	V.flatten(flattened_f);
	V_residual.set(complex<T>(T(flattened_f[0]), T(flattened_f[1])),
			complex<T>(T(flattened_f[2]), T(flattened_f[3])),
			complex<T>(T(flattened_f[4]), T(flattened_f[5])),
			complex<T>(T(flattened_f[6]), T(flattened_f[7])));	// V_residual = V
	P.flatten(flattened_f);
	Pt.set(complex<T>(T(flattened_f[0]), T(flattened_f[1])),
			complex<T>(T(flattened_f[2]), T(flattened_f[3])),
			complex<T>(T(flattened_f[4]), T(flattened_f[5])),
			complex<T>(T(flattened_f[6]), T(flattened_f[7])));	
	V_residual -= Jj*Pt*Jk.conjugate_transpose();				// V_residual = V-JjPJk^
    
	V_residual.flatten(flattened_T);
	for (int i=0; i<8; ++i) {
            residual[i] = flattened_T[i];
        }
    return true;
  }

 private:
  const JonesMatrix<float> V;
  const JonesMatrix<float> P;
  const int st1;
  const int st2;
};

void Optimize::run() {
  switch( stands.size() ) {
    case 3 : run3(); break;
    case 4 : run4(); break;
    case 5 : run5(); break;
    case 6 : run6(); break;
    case 7 : run7(); break;
    case 8 : run8(); break;
    case 9 : run9(); break;
    case 10 : run10(); break;
    case 11 : run11(); break;
    case 12 : run12(); break;
    case 13 : run13(); break;
    case 14 : run14(); break;
    case 15 : run15(); break;
    case 16 : run16(); break;
    case 17 : run17(); break;
    case 18 : run18(); break;
    case 19 : run19(); break;
    case 20 : run20(); break;
    case 21 : run21(); break;
    case 22 : run22(); break;
    case 23 : run23(); break;
    case 24 : run24(); break;
    case 25 : run25(); break;
    case 26 : run26(); break;
    case 27 : run27(); break;
    case 28 : run28(); break;
    case 29 : run29(); break;
    case 30 : run30(); break;
    case 31 : run31(); break;
    case 32 : run32(); break;
    case 33 : run33(); break;
    case 34 : run34(); break;
    case 35 : run35(); break;
    case 36 : run36(); break;
    case 37 : run37(); break;
    case 38 : run38(); break;
    case 39 : run39(); break;
    case 40 : run40(); break;
    case 41 : run41(); break;
    case 42 : run42(); break;
    case 43 : run43(); break;
    case 44 : run44(); break;
    case 45 : run45(); break;
    case 46 : run46(); break;
    case 47 : run47(); break;
    case 48 : run48(); break;
    case 49 : run49(); break;
    case 50 : run50(); break;
    case 51 : run51(); break;
    case 52 : run52(); break;
    case 53 : run53(); break;
    case 54 : run54(); break;
    case 55 : run55(); break;
    case 56 : run56(); break;
    case 57 : run57(); break;
    case 58 : run58(); break;
    case 59 : run59(); break;
    case 60 : run60(); break;
    case 61 : run61(); break;
    case 62 : run62(); break;
    case 63 : run63(); break;
    case 64 : run64(); break;
    case 65 : run65(); break;
    case 66 : run66(); break;
    case 67 : run67(); break;
    case 68 : run68(); break;
    case 69 : run69(); break;
    case 70 : run70(); break;
    case 71 : run71(); break;
    case 72 : run72(); break;
    case 73 : run73(); break;
    case 74 : run74(); break;
    case 75 : run75(); break;
    case 76 : run76(); break;
    case 77 : run77(); break;
    case 78 : run78(); break;
    case 79 : run79(); break;
    case 80 : run80(); break;
    case 81 : run81(); break;
    case 82 : run82(); break;
    case 83 : run83(); break;
    case 84 : run84(); break;
    case 85 : run85(); break;
    case 86 : run86(); break;
    case 87 : run87(); break;
    case 88 : run88(); break;
    case 89 : run89(); break;
    case 90 : run90(); break;
    case 91 : run91(); break;
    case 92 : run92(); break;
    case 93 : run93(); break;
    case 94 : run94(); break;
    case 95 : run95(); break;
    case 96 : run96(); break;
    case 97 : run97(); break;
    case 98 : run98(); break;
    case 99 : run99(); break;
    case 100 : run100(); break;
    case 101 : run101(); break;
    case 102 : run102(); break;
    case 103 : run103(); break;
    case 104 : run104(); break;
    case 105 : run105(); break;
    case 106 : run106(); break;
    case 107 : run107(); break;
    case 108 : run108(); break;
    case 109 : run109(); break;
    case 110 : run110(); break;
    case 111 : run111(); break;
    case 112 : run112(); break;
    case 113 : run113(); break;
    case 114 : run114(); break;
    case 115 : run115(); break;
    case 116 : run116(); break;
    case 117 : run117(); break;
    case 118 : run118(); break;
    case 119 : run119(); break;
    case 120 : run120(); break;
    case 121 : run121(); break;
    case 122 : run122(); break;
    case 123 : run123(); break;
    case 124 : run124(); break;
    case 125 : run125(); break;
    case 126 : run126(); break;
    case 127 : run127(); break;
    case 128 : run128(); break;
    case 129 : run129(); break;
    case 130 : run130(); break;
    case 131 : run131(); break;
    case 132 : run132(); break;
    case 133 : run133(); break;
    case 134 : run134(); break;
    case 135 : run135(); break;
    case 136 : run136(); break;
    case 137 : run137(); break;
    case 138 : run138(); break;
    case 139 : run139(); break;
    case 140 : run140(); break;
    case 141 : run141(); break;
    case 142 : run142(); break;
    case 143 : run143(); break;
    case 144 : run144(); break;
    case 145 : run145(); break;
    case 146 : run146(); break;
    case 147 : run147(); break;
    case 148 : run148(); break;
    case 149 : run149(); break;
    case 150 : run150(); break;
    case 151 : run151(); break;
    case 152 : run152(); break;
    case 153 : run153(); break;
    case 154 : run154(); break;
    case 155 : run155(); break;
    case 156 : run156(); break;
    case 157 : run157(); break;
    case 158 : run158(); break;
    case 159 : run159(); break;
    case 160 : run160(); break;
    case 161 : run161(); break;
    case 162 : run162(); break;
    case 163 : run163(); break;
    case 164 : run164(); break;
    case 165 : run165(); break;
    case 166 : run166(); break;
    case 167 : run167(); break;
    case 168 : run168(); break;
    case 169 : run169(); break;
    case 170 : run170(); break;
    case 171 : run171(); break;
    case 172 : run172(); break;
    case 173 : run173(); break;
    case 174 : run174(); break;
    case 175 : run175(); break;
    case 176 : run176(); break;
    case 177 : run177(); break;
    case 178 : run178(); break;
    case 179 : run179(); break;
    case 180 : run180(); break;
    case 181 : run181(); break;
    case 182 : run182(); break;
    case 183 : run183(); break;
    case 184 : run184(); break;
    case 185 : run185(); break;
    case 186 : run186(); break;
    case 187 : run187(); break;
    case 188 : run188(); break;
    case 189 : run189(); break;
    case 190 : run190(); break;
    case 191 : run191(); break;
    case 192 : run192(); break;
    case 193 : run193(); break;
    case 194 : run194(); break;
    case 195 : run195(); break;
    case 196 : run196(); break;
    case 197 : run197(); break;
    case 198 : run198(); break;
    case 199 : run199(); break;
    case 200 : run200(); break;
    case 201 : run201(); break;
    case 202 : run202(); break;
    case 203 : run203(); break;
    case 204 : run204(); break;
    case 205 : run205(); break;
    case 206 : run206(); break;
    case 207 : run207(); break;
    case 208 : run208(); break;
    case 209 : run209(); break;
    case 210 : run210(); break;
    case 211 : run211(); break;
    case 212 : run212(); break;
    case 213 : run213(); break;
    case 214 : run214(); break;
    case 215 : run215(); break;
    case 216 : run216(); break;
    case 217 : run217(); break;
    case 218 : run218(); break;
    case 219 : run219(); break;
    case 220 : run220(); break;
    case 221 : run221(); break;
    case 222 : run222(); break;
    case 223 : run223(); break;
    case 224 : run224(); break;
    case 225 : run225(); break;
    case 226 : run226(); break;
    case 227 : run227(); break;
    case 228 : run228(); break;
    case 229 : run229(); break;
    case 230 : run230(); break;
    case 231 : run231(); break;
    case 232 : run232(); break;
    case 233 : run233(); break;
    case 234 : run234(); break;
    case 235 : run235(); break;
    case 236 : run236(); break;
    case 237 : run237(); break;
    case 238 : run238(); break;
    case 239 : run239(); break;
    case 240 : run240(); break;
    case 241 : run241(); break;
    case 242 : run242(); break;
    case 243 : run243(); break;
    case 244 : run244(); break;
    case 245 : run245(); break;
    case 246 : run246(); break;
    case 247 : run247(); break;
    case 248 : run248(); break;
    case 249 : run249(); break;
    case 250 : run250(); break;

    default: 
	cerr << "Can't run optimize on " << stands.size() << " stands\n"; exit(1);
  }
}
