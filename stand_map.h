/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <vector>
using namespace std;

class StandMapping {
public:
  vector<int> mapping;

  void add(int st);
  void add(vector<int> v) { for (int i=0; i<v.size(); ++i) add(v[i]); }
  void add(int *a, int size) {  for (int i=0; i<size; ++i) add(a[i]); }
  int map(int st);
  int num_mapped() { return mapping.size(); }
  void clear() { mapping.clear(); }
  void print() { for (int i=0; i<mapping.size(); ++i) cout << i << " -> " << mapping[i] << endl; }
};

