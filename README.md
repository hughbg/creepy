# README #

Creeping Optimization for calibration in radio interferometry. The aim is to avoid calibrating  the entire telescope at one attempt, but to "creep up" on the calibration, incorporating more and more antennas. This is useful when part of the telescope is faulty - the aim is to intelligently avoid the faulty part.


The program does  a search to find the 3 antennas that calibrate the best, then adds antennas one by one to the set, discarding antennas that make things worse. By "calibration" I mean gain calibration against a sky model, which must be supplied. Visibilities and sky model are  passed into the program by way of a file, which requires a specific binary format (contact me about that: hughgarsden at yahoo.com). The result of the optimization is a set of Jones matrices that align the visibilities to the sky.

The program uses the [Ceres solver](http://ceres-solver.org) for optimization, and multi-threading for performance.

The program is not yet ready  for public use, and is in the experimental stage.
