/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "stand_map.h"

void StandMapping::add(int st) {

   for (int i=0; i< mapping.size(); ++i)
     if ( mapping[i] == st ) return;	// already in
   mapping.push_back(st);
}


int StandMapping::map(int st) {
	for (int i=0; i<mapping.size(); ++i) 
		if ( i == st ) return mapping[st];
	return -1;
}
