/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <cstdlib>
#include <fstream>
#include <algorithm>
#include "baselines.h"

static vector<string> split(string line) {
   vector<string> tokens;
   string token;

   for (int i=0; i<line.size(); ++i) if ( line[i] == '\t' ) line[i] = ' ';
   stringstream l(line);

   while ( getline(l, token, ' ') )
     if ( token.size() > 0 ) tokens.push_back(token);

   return tokens;
}

Baseline::Baseline(const Baseline& bl) {
  st1 = bl.st1;
  st2 = bl.st2;
  vis = bl.vis;
  model = bl.model; 
}

Baseline& Baseline::operator=(const Baseline& bl) {
  st1 = bl.st1;
  st2 = bl.st2;
  vis = bl.vis;
  model = bl.model;
  has_data = bl.has_data;
  return *this;
}



Baseline::Baseline(int s1, int s2, float values[16]) {
    st1 = s1; st2 = s2;

    complex<float> meas0 = complex<float>(values[0], values[1]);
    complex<float> meas1 = complex<float>(values[2], values[3]);
    complex<float> meas2 = complex<float>(values[4], values[5]);
    complex<float> meas3 = complex<float>(values[6], values[7]);

    complex<float> model0 = complex<float>(values[8], values[9]);
    complex<float> model1 = complex<float>(values[10], values[11]);
    complex<float> model2 = complex<float>(values[12], values[13]);
    complex<float> model3 = complex<float>(values[14], values[15]);


    vis.set(meas0, meas1, meas2, meas3);
    model.set(model0, model1, model2, model3);

    if ( vis.norm() == 0 || model.norm() == 0 ) has_data = false;
    else has_data = true;
}

static void read_jones(ifstream& inf, JonesMatrix<float>& jones) {
	float re, im;
    complex <float> c[4];
	for (int p=0; p<4; ++p) {
		inf.read((char*)&re, sizeof(float)); inf.read((char*)&im, sizeof(float));
		c[p] = complex<float>(re, im);
	}
	jones.set(c[0], c[1], c[2], c[3]);
}

Baselines::Baselines(const char *fname) {
    string line;
    bool done;
    vector<string> words;
    
    for (int i=0; i<NUM_STANDS; ++i) for (int j=0; j<NUM_STANDS; ++j) location[i][j] = -1;

    // Find out how many
    ifstream file1(fname, ios::in | ios::binary);
    
    // Get Jones
    if ( (init_jones=new JonesMatrix<float>[NUM_STANDS]) == NULL ) {
    	cerr << "Failed to allocate memory for init_jones\n";
    	exit(1);
    }
    for (int i=0; i<NUM_STANDS; ++i) read_jones(file1, init_jones[i]); 

    num_baselines = 0; done = false;
    while( !done ) {
    	char junk[sizeof(int)*2+sizeof(float)*16];
    	file1.read(junk, sizeof(int)*2+sizeof(float)*16);
    	if ( !file1 ) done = true;
    	else ++num_baselines;
    }
    file1.close(); 

    if ( (baselines = new Baseline[num_baselines]) == NULL ) {
     cerr << "failed to allocate memory for baselines\n";
     exit(1);
    }

   // Get baselines
   ifstream file2(fname,  ios::in | ios::binary);
   
   // Get Jones
   for (int i=0; i<NUM_STANDS; ++i) read_jones(file2, init_jones[i]);

   for (int bl=0; bl<num_baselines; ++bl) {
	   int st1, st2;
	   float values[16];
	   file2.read((char*)&st1, sizeof(int)); file2.read((char*)&st2, sizeof(int));
	   file2.read((char*)&values[0], sizeof(float)*16);
	   baselines[bl] = Baseline(st1, st2, values);
	   location[st1][st2] = bl;
   }
   file2.close(); 

   for (int i=0; i<num_baselines; ++i) { 
       if ( !in(baselines[i].st1) ) stands_present.push_back(baselines[i].st1);
       if ( !in(baselines[i].st2) ) stands_present.push_back(baselines[i].st2);
   }

   std::sort(stands_present.begin(), stands_present.end());

    
   cout << stands_present.size() << " stands present\n"; 
   
}

int Baselines::find(int st1, int st2) {
	int where;
	if ( (where=location[st1][st2]) == -1 ) return -1;
	else return where;
}
