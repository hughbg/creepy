/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <vector>
using namespace std;

#define MAX_THREADS 32
#define WORK_PER_THREAD 512

class Work {
public:
  vector<int> work[MAX_THREADS][WORK_PER_THREAD];

  void add(vector<int> stands);
  bool full();
  void clear() { for (int i=0; i<MAX_THREADS; ++i) for (int j=0; j<WORK_PER_THREAD; ++j) work[i][j].clear(); }
};
