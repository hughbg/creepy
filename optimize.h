/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "baselines.h"
#include "stand_map.h"

class Optimize {
public:
  vector<int> stands;
  int levmar_result;
  JonesMatrix<float> **V, **P, *J;

  Optimize(StandMapping& stands_map, Baselines& baselines, JonesMatrix<float> *J_cur);
  ~Optimize();
  float residual();
  void residual_for_opt(double*);

  void run();
  void run3();
  void run4();
  void run5();
  void run6();
  void run7();
  void run8();
  void run9();
  void run10();
  void run11();
  void run12();
  void run13();
  void run14();
  void run15();
  void run16();
  void run17();
  void run18();
  void run19();
  void run20();
  void run21();
  void run22();
  void run23();
  void run24();
  void run25();
  void run26();
  void run27();
  void run28();
  void run29();
  void run30();
  void run31();
  void run32();
  void run33();
  void run34();
  void run35();
  void run36();
  void run37();
  void run38();
  void run39();
  void run40();
  void run41();
  void run42();
  void run43();
  void run44();
  void run45();
  void run46();
  void run47();
  void run48();
  void run49();
  void run50();
  void run51();
  void run52();
  void run53();
  void run54();
  void run55();
  void run56();
  void run57();
  void run58();
  void run59();
  void run60();
  void run61();
  void run62();
  void run63();
  void run64();
  void run65();
  void run66();
  void run67();
  void run68();
  void run69();
  void run70();
  void run71();
  void run72();
  void run73();
  void run74();
  void run75();
  void run76();
  void run77();
  void run78();
  void run79();
  void run80();
  void run81();
  void run82();
  void run83();
  void run84();
  void run85();
  void run86();
  void run87();
  void run88();
  void run89();
  void run90();
  void run91();
  void run92();
  void run93();
  void run94();
  void run95();
  void run96();
  void run97();
  void run98();
  void run99();
  void run100();
  void run101();
  void run102();
  void run103();
  void run104();
  void run105();
  void run106();
  void run107();
  void run108();
  void run109();
  void run110();
  void run111();
  void run112();
  void run113();
  void run114();
  void run115();
  void run116();
  void run117();
  void run118();
  void run119();
  void run120();
  void run121();
  void run122();
  void run123();
  void run124();
  void run125();
  void run126();
  void run127();
  void run128();
  void run129();
  void run130();
  void run131();
  void run132();
  void run133();
  void run134();
  void run135();
  void run136();
  void run137();
  void run138();
  void run139();
  void run140();
  void run141();
  void run142();
  void run143();
  void run144();
  void run145();
  void run146();
  void run147();
  void run148();
  void run149();
  void run150();
  void run151();
  void run152();
  void run153();
  void run154();
  void run155();
  void run156();
  void run157();
  void run158();
  void run159();
  void run160();
  void run161();
  void run162();
  void run163();
  void run164();
  void run165();
  void run166();
  void run167();
  void run168();
  void run169();
  void run170();
  void run171();
  void run172();
  void run173();
  void run174();
  void run175();
  void run176();
  void run177();
  void run178();
  void run179();
  void run180();
  void run181();
  void run182();
  void run183();
  void run184();
  void run185();
  void run186();
  void run187();
  void run188();
  void run189();
  void run190();
  void run191();
  void run192();
  void run193();
  void run194();
  void run195();
  void run196();
  void run197();
  void run198();
  void run199();
  void run200();
  void run201();
  void run202();
  void run203();
  void run204();
  void run205();
  void run206();
  void run207();
  void run208();
  void run209();
  void run210();
  void run211();
  void run212();
  void run213();
  void run214();
  void run215();
  void run216();
  void run217();
  void run218();
  void run219();
  void run220();
  void run221();
  void run222();
  void run223();
  void run224();
  void run225();
  void run226();
  void run227();
  void run228();
  void run229();
  void run230();
  void run231();
  void run232();
  void run233();
  void run234();
  void run235();
  void run236();
  void run237();
  void run238();
  void run239();
  void run240();
  void run241();
  void run242();
  void run243();
  void run244();
  void run245();
  void run246();
  void run247();
  void run248();
  void run249();
  void run250();

};
