/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <cstdlib>
#include "optimize.h"
#include "ceres/ceres.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;


Optimize::Optimize(StandMapping& stands_mapping, Baselines& baselines, JonesMatrix<float> *J_cur) {
	stands = stands_mapping.mapping;
	levmar_result = -1;
	
	J = new JonesMatrix<float>[stands.size()];
	if ( J == NULL ) {
          cerr  << "Failed to allocate J in Optimize\n";
          exit(1);
	}
	V = new JonesMatrix<float>*[stands.size()];
        if ( V == NULL ) {
          cerr  << "Failed to allocate V in Optimize\n";
          exit(1);
        }
	for (int i=0; i<stands.size(); ++i) {
	  V[i] = new JonesMatrix<float>[stands.size()];
          if ( V[i] == NULL ) {
            cerr  << "Failed to allocate V[i] in Optimize\n";
            exit(1);
          }
        }
	P = new JonesMatrix<float>*[stands.size()];
        if ( P == NULL ) {
          cerr  << "Failed to allocate P in Optimize\n";
          exit(1);
        }
	for (int i=0; i<stands.size(); ++i) {
	  P[i] = new JonesMatrix<float>[stands.size()];
          if ( P[i] == NULL ) {
            cerr  << "Failed to allocate P[i] in Optimize\n";
            exit(1);
          }
        }
	for (int i=0; i<stands.size(); ++i) {
		int st1 = stands[i]; 
		J[i] = J_cur[st1]; 
		for (int j=i+1; j<stands.size(); ++j) {
			int st2 = stands[j];
			if ( st2 == -1 ) {
				cerr << "Consistency err: asked to map invalid stand in Optimize\n";
				exit(1);
			}
			int bl_index = baselines.find(st1, st2);
			if ( bl_index != -1 ) {
       

				V[i][j] = baselines.baselines[bl_index].vis;     
				P[i][j] = baselines.baselines[bl_index].model;
				
        
			} else {
				V[i][j].in_use = false;
				P[i][j].in_use = false;
			}
		} 
	}

}    

Optimize::~Optimize() {
  delete[] J;
  
  for (int i=0; i<stands.size(); ++i) { delete[] V[i]; delete[] P[i]; }
  delete[] V; delete[] P;
 
}


float Optimize::residual() {  // Between V and J.P.Jt
  double res = 0;
  float V_sum=0, V_residual=0; 
  
  for (int j=0; j<stands.size(); ++j) {
    for (int k=j+1; k<stands.size(); ++k) {    // Not using the whole thing
    	//if ( squared_norm(V[j][k]) == 0 ) cout << " x ";
    	//else cout << " . ";
    	if ( V[j][k].in_use  ) {
    		V_sum += V[j][k].norm();
    
    		//cout << "----" << j << " " << k << endl;
    		//V[j][k].print(); J[j].print(); J[k].print(); P[j][k].print(); (J[j]*P[j][k]*J[k].conjugate_transpose()).print(); 
    		V_residual += (V[j][k]-(J[j]*P[j][k]*J[k].conjugate_transpose())).norm();
    	}
    }
    //cout << endl;
  }
  return  V_residual/V_sum;  
}

struct BaselineResidual {

  BaselineResidual(JonesMatrix<float> V_, JonesMatrix<float> P_, int st1_, int st2_) 
      : V(V_), P(P_), st1(st1_), st2(st2_) {
	  
	  //cout << st1 << " " << st2 << endl;
	  //V.print(); P.print();
  }

  template <typename T> 
  bool operator()(const T* const jones_vals, T* residual) const {
	  float flattened_f[8];
	  T flattened_T[8];

	JonesMatrix<T> Jj, Jk; 
	Jj.set(complex<T>(jones_vals[st1*8+0], jones_vals[st1*8+1]), 
			complex<T>(jones_vals[st1*8+2], jones_vals[st1*8+3]), 
			complex<T>(jones_vals[st1*8+4], jones_vals[st1*8+5]), 
			complex<T>(jones_vals[st1*8+6], jones_vals[st1*8+7]));
	Jk.set(complex<T>(jones_vals[st2*8+0], jones_vals[st2*8+1]), 
		complex<T>(jones_vals[st2*8+2], jones_vals[st2*8+3]), 
		complex<T>(jones_vals[st2*8+4], jones_vals[st2*8+5]), 
		complex<T>(jones_vals[st2*8+6], jones_vals[st2*8+7]));
        
	JonesMatrix<T> V_residual, Pt;
	V.flatten(flattened_f);
	V_residual.set(complex<T>(T(flattened_f[0]), T(flattened_f[1])),
			complex<T>(T(flattened_f[2]), T(flattened_f[3])),
			complex<T>(T(flattened_f[4]), T(flattened_f[5])),
			complex<T>(T(flattened_f[6]), T(flattened_f[7])));	// V_residual = V
	P.flatten(flattened_f);
	Pt.set(complex<T>(T(flattened_f[0]), T(flattened_f[1])),
			complex<T>(T(flattened_f[2]), T(flattened_f[3])),
			complex<T>(T(flattened_f[4]), T(flattened_f[5])),
			complex<T>(T(flattened_f[6]), T(flattened_f[7])));	
	V_residual -= Jj*Pt*Jk.conjugate_transpose();				// V_residual = V-JjPJk^
    
	V_residual.flatten(flattened_T);
	for (int i=0; i<8; ++i) {
            residual[i] = flattened_T[i];
        }
    return true;
  }

 private:
  const JonesMatrix<float> V;
  const JonesMatrix<float> P;
  const int st1;
  const int st2;
};

void Optimize::run() {
  switch( stands.size() ) {
    case 3 : run3(); break;
    case 4 : run4(); break;
    case 5 : run5(); break;
    case 6 : run6(); break;
    case 7 : run7(); break;
    case 8 : run8(); break;
    case 9 : run9(); break;
    case 10 : run10(); break;
    case 11 : run11(); break;
    case 12 : run12(); break;
    case 13 : run13(); break;
    case 14 : run14(); break;
    case 15 : run15(); break;
    case 16 : run16(); break;
    case 17 : run17(); break;
    case 18 : run18(); break;
    case 19 : run19(); break;
    case 20 : run20(); break;
    case 21 : run21(); break;
    case 22 : run22(); break;
    case 23 : run23(); break;
    case 24 : run24(); break;
    case 25 : run25(); break;
    case 26 : run26(); break;
    case 27 : run27(); break;
    case 28 : run28(); break;
    case 29 : run29(); break;
    case 30 : run30(); break;
    case 31 : run31(); break;
    case 32 : run32(); break;
    case 33 : run33(); break;
    case 34 : run34(); break;
    case 35 : run35(); break;
    case 36 : run36(); break;
    case 37 : run37(); break;
    case 38 : run38(); break;
    case 39 : run39(); break;
    case 40 : run40(); break;
    case 41 : run41(); break;
    case 42 : run42(); break;
    case 43 : run43(); break;
    case 44 : run44(); break;
    case 45 : run45(); break;
    case 46 : run46(); break;
    case 47 : run47(); break;
    case 48 : run48(); break;
    case 49 : run49(); break;
    case 50 : run50(); break;
    case 51 : run51(); break;
    case 52 : run52(); break;
    case 53 : run53(); break;
    case 54 : run54(); break;
    case 55 : run55(); break;
    case 56 : run56(); break;
    case 57 : run57(); break;
    case 58 : run58(); break;
    case 59 : run59(); break;
    case 60 : run60(); break;
    case 61 : run61(); break;
    case 62 : run62(); break;
    case 63 : run63(); break;
    case 64 : run64(); break;
    case 65 : run65(); break;
    case 66 : run66(); break;
    case 67 : run67(); break;
    case 68 : run68(); break;
    case 69 : run69(); break;
    case 70 : run70(); break;
    case 71 : run71(); break;
    case 72 : run72(); break;
    case 73 : run73(); break;
    case 74 : run74(); break;
    case 75 : run75(); break;
    case 76 : run76(); break;
    case 77 : run77(); break;
    case 78 : run78(); break;
    case 79 : run79(); break;
    case 80 : run80(); break;
    case 81 : run81(); break;
    case 82 : run82(); break;
    case 83 : run83(); break;
    case 84 : run84(); break;
    case 85 : run85(); break;
    case 86 : run86(); break;
    case 87 : run87(); break;
    case 88 : run88(); break;
    case 89 : run89(); break;
    case 90 : run90(); break;
    case 91 : run91(); break;
    case 92 : run92(); break;
    case 93 : run93(); break;
    case 94 : run94(); break;
    case 95 : run95(); break;
    case 96 : run96(); break;
    case 97 : run97(); break;
    case 98 : run98(); break;
    case 99 : run99(); break;
    case 100 : run100(); break;
    case 101 : run101(); break;
    case 102 : run102(); break;
    case 103 : run103(); break;
    case 104 : run104(); break;
    case 105 : run105(); break;
    case 106 : run106(); break;
    case 107 : run107(); break;
    case 108 : run108(); break;
    case 109 : run109(); break;
    case 110 : run110(); break;
    case 111 : run111(); break;
    case 112 : run112(); break;
    case 113 : run113(); break;
    case 114 : run114(); break;
    case 115 : run115(); break;
    case 116 : run116(); break;
    case 117 : run117(); break;
    case 118 : run118(); break;
    case 119 : run119(); break;
    case 120 : run120(); break;
    case 121 : run121(); break;
    case 122 : run122(); break;
    case 123 : run123(); break;
    case 124 : run124(); break;
    case 125 : run125(); break;
    case 126 : run126(); break;
    case 127 : run127(); break;
    case 128 : run128(); break;
    case 129 : run129(); break;
    case 130 : run130(); break;
    case 131 : run131(); break;
    case 132 : run132(); break;
    case 133 : run133(); break;
    case 134 : run134(); break;
    case 135 : run135(); break;
    case 136 : run136(); break;
    case 137 : run137(); break;
    case 138 : run138(); break;
    case 139 : run139(); break;
    case 140 : run140(); break;
    case 141 : run141(); break;
    case 142 : run142(); break;
    case 143 : run143(); break;
    case 144 : run144(); break;
    case 145 : run145(); break;
    case 146 : run146(); break;
    case 147 : run147(); break;
    case 148 : run148(); break;
    case 149 : run149(); break;
    case 150 : run150(); break;
    case 151 : run151(); break;
    case 152 : run152(); break;
    case 153 : run153(); break;
    case 154 : run154(); break;
    case 155 : run155(); break;
    case 156 : run156(); break;
    case 157 : run157(); break;
    case 158 : run158(); break;
    case 159 : run159(); break;
    case 160 : run160(); break;
    case 161 : run161(); break;
    case 162 : run162(); break;
    case 163 : run163(); break;
    case 164 : run164(); break;
    case 165 : run165(); break;
    case 166 : run166(); break;
    case 167 : run167(); break;
    case 168 : run168(); break;
    case 169 : run169(); break;
    case 170 : run170(); break;
    case 171 : run171(); break;
    case 172 : run172(); break;
    case 173 : run173(); break;
    case 174 : run174(); break;
    case 175 : run175(); break;
    case 176 : run176(); break;
    case 177 : run177(); break;
    case 178 : run178(); break;
    case 179 : run179(); break;
    case 180 : run180(); break;
    case 181 : run181(); break;
    case 182 : run182(); break;
    case 183 : run183(); break;
    case 184 : run184(); break;
    case 185 : run185(); break;
    case 186 : run186(); break;
    case 187 : run187(); break;
    case 188 : run188(); break;
    case 189 : run189(); break;
    case 190 : run190(); break;
    case 191 : run191(); break;
    case 192 : run192(); break;
    case 193 : run193(); break;
    case 194 : run194(); break;
    case 195 : run195(); break;
    case 196 : run196(); break;
    case 197 : run197(); break;
    case 198 : run198(); break;
    case 199 : run199(); break;
    case 200 : run200(); break;
    case 201 : run201(); break;
    case 202 : run202(); break;
    case 203 : run203(); break;
    case 204 : run204(); break;
    case 205 : run205(); break;
    case 206 : run206(); break;
    case 207 : run207(); break;
    case 208 : run208(); break;
    case 209 : run209(); break;
    case 210 : run210(); break;
    case 211 : run211(); break;
    case 212 : run212(); break;
    case 213 : run213(); break;
    case 214 : run214(); break;
    case 215 : run215(); break;
    case 216 : run216(); break;
    case 217 : run217(); break;
    case 218 : run218(); break;
    case 219 : run219(); break;
    case 220 : run220(); break;
    case 221 : run221(); break;
    case 222 : run222(); break;
    case 223 : run223(); break;
    case 224 : run224(); break;
    case 225 : run225(); break;
    case 226 : run226(); break;
    case 227 : run227(); break;
    case 228 : run228(); break;
    case 229 : run229(); break;
    case 230 : run230(); break;
    case 231 : run231(); break;
    case 232 : run232(); break;
    case 233 : run233(); break;
    case 234 : run234(); break;
    case 235 : run235(); break;
    case 236 : run236(); break;
    case 237 : run237(); break;
    case 238 : run238(); break;
    case 239 : run239(); break;
    case 240 : run240(); break;
    case 241 : run241(); break;
    case 242 : run242(); break;
    case 243 : run243(); break;
    case 244 : run244(); break;
    case 245 : run245(); break;
    case 246 : run246(); break;
    case 247 : run247(); break;
    case 248 : run248(); break;
    case 249 : run249(); break;
    case 250 : run250(); break;

    default: 
	cerr << "Can't run optimize on " << stands.size() << " stands\n"; exit(1);
  }
}
void Optimize::run3() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 3 ) {
        cerr << "Wrong number of stands to function run" << 3 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 3*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;
problem.EvaluateOptions.num_threads;
	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run4() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 4 ) {
        cerr << "Wrong number of stands to function run" << 4 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 4*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run5() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 5 ) {
        cerr << "Wrong number of stands to function run" << 5 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 5*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run6() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 6 ) {
        cerr << "Wrong number of stands to function run" << 6 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 6*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run7() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 7 ) {
        cerr << "Wrong number of stands to function run" << 7 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 7*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run8() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 8 ) {
        cerr << "Wrong number of stands to function run" << 8 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 8*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run9() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 9 ) {
        cerr << "Wrong number of stands to function run" << 9 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 9*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run10() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 10 ) {
        cerr << "Wrong number of stands to function run" << 10 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 10*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run11() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 11 ) {
        cerr << "Wrong number of stands to function run" << 11 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 11*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run12() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 12 ) {
        cerr << "Wrong number of stands to function run" << 12 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 12*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run13() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 13 ) {
        cerr << "Wrong number of stands to function run" << 13 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 13*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run14() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 14 ) {
        cerr << "Wrong number of stands to function run" << 14 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 14*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run15() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 15 ) {
        cerr << "Wrong number of stands to function run" << 15 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 15*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run16() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 16 ) {
        cerr << "Wrong number of stands to function run" << 16 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 16*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run17() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 17 ) {
        cerr << "Wrong number of stands to function run" << 17 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 17*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run18() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 18 ) {
        cerr << "Wrong number of stands to function run" << 18 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 18*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run19() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 19 ) {
        cerr << "Wrong number of stands to function run" << 19 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 19*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run20() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 20 ) {
        cerr << "Wrong number of stands to function run" << 20 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 20*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run21() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 21 ) {
        cerr << "Wrong number of stands to function run" << 21 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 21*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run22() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 22 ) {
        cerr << "Wrong number of stands to function run" << 22 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 22*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run23() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 23 ) {
        cerr << "Wrong number of stands to function run" << 23 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 23*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run24() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 24 ) {
        cerr << "Wrong number of stands to function run" << 24 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 24*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run25() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 25 ) {
        cerr << "Wrong number of stands to function run" << 25 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 25*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run26() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 26 ) {
        cerr << "Wrong number of stands to function run" << 26 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 26*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run27() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 27 ) {
        cerr << "Wrong number of stands to function run" << 27 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 27*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run28() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 28 ) {
        cerr << "Wrong number of stands to function run" << 28 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 28*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run29() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 29 ) {
        cerr << "Wrong number of stands to function run" << 29 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 29*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run30() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 30 ) {
        cerr << "Wrong number of stands to function run" << 30 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 30*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run31() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 31 ) {
        cerr << "Wrong number of stands to function run" << 31 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 31*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run32() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 32 ) {
        cerr << "Wrong number of stands to function run" << 32 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 32*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run33() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 33 ) {
        cerr << "Wrong number of stands to function run" << 33 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 33*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run34() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 34 ) {
        cerr << "Wrong number of stands to function run" << 34 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 34*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run35() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 35 ) {
        cerr << "Wrong number of stands to function run" << 35 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 35*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run36() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 36 ) {
        cerr << "Wrong number of stands to function run" << 36 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 36*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run37() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 37 ) {
        cerr << "Wrong number of stands to function run" << 37 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 37*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run38() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 38 ) {
        cerr << "Wrong number of stands to function run" << 38 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 38*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run39() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 39 ) {
        cerr << "Wrong number of stands to function run" << 39 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 39*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run40() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 40 ) {
        cerr << "Wrong number of stands to function run" << 40 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 40*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run41() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 41 ) {
        cerr << "Wrong number of stands to function run" << 41 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 41*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run42() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 42 ) {
        cerr << "Wrong number of stands to function run" << 42 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 42*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run43() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 43 ) {
        cerr << "Wrong number of stands to function run" << 43 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 43*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run44() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 44 ) {
        cerr << "Wrong number of stands to function run" << 44 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 44*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run45() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 45 ) {
        cerr << "Wrong number of stands to function run" << 45 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 45*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run46() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 46 ) {
        cerr << "Wrong number of stands to function run" << 46 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 46*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run47() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 47 ) {
        cerr << "Wrong number of stands to function run" << 47 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 47*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run48() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 48 ) {
        cerr << "Wrong number of stands to function run" << 48 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 48*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run49() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 49 ) {
        cerr << "Wrong number of stands to function run" << 49 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 49*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run50() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 50 ) {
        cerr << "Wrong number of stands to function run" << 50 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 50*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run51() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 51 ) {
        cerr << "Wrong number of stands to function run" << 51 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 51*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run52() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 52 ) {
        cerr << "Wrong number of stands to function run" << 52 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 52*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run53() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 53 ) {
        cerr << "Wrong number of stands to function run" << 53 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 53*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run54() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 54 ) {
        cerr << "Wrong number of stands to function run" << 54 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 54*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run55() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 55 ) {
        cerr << "Wrong number of stands to function run" << 55 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 55*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run56() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 56 ) {
        cerr << "Wrong number of stands to function run" << 56 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 56*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run57() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 57 ) {
        cerr << "Wrong number of stands to function run" << 57 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 57*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run58() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 58 ) {
        cerr << "Wrong number of stands to function run" << 58 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 58*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run59() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 59 ) {
        cerr << "Wrong number of stands to function run" << 59 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 59*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run60() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 60 ) {
        cerr << "Wrong number of stands to function run" << 60 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 60*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run61() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 61 ) {
        cerr << "Wrong number of stands to function run" << 61 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 61*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run62() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 62 ) {
        cerr << "Wrong number of stands to function run" << 62 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 62*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run63() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 63 ) {
        cerr << "Wrong number of stands to function run" << 63 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 63*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run64() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 64 ) {
        cerr << "Wrong number of stands to function run" << 64 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 64*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run65() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 65 ) {
        cerr << "Wrong number of stands to function run" << 65 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 65*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run66() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 66 ) {
        cerr << "Wrong number of stands to function run" << 66 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 66*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run67() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 67 ) {
        cerr << "Wrong number of stands to function run" << 67 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 67*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run68() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 68 ) {
        cerr << "Wrong number of stands to function run" << 68 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 68*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run69() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 69 ) {
        cerr << "Wrong number of stands to function run" << 69 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 69*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run70() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 70 ) {
        cerr << "Wrong number of stands to function run" << 70 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 70*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run71() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 71 ) {
        cerr << "Wrong number of stands to function run" << 71 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 71*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run72() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 72 ) {
        cerr << "Wrong number of stands to function run" << 72 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 72*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run73() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 73 ) {
        cerr << "Wrong number of stands to function run" << 73 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 73*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run74() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 74 ) {
        cerr << "Wrong number of stands to function run" << 74 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 74*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run75() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 75 ) {
        cerr << "Wrong number of stands to function run" << 75 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 75*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run76() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 76 ) {
        cerr << "Wrong number of stands to function run" << 76 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 76*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run77() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 77 ) {
        cerr << "Wrong number of stands to function run" << 77 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 77*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run78() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 78 ) {
        cerr << "Wrong number of stands to function run" << 78 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 78*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run79() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 79 ) {
        cerr << "Wrong number of stands to function run" << 79 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 79*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run80() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 80 ) {
        cerr << "Wrong number of stands to function run" << 80 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 80*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run81() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 81 ) {
        cerr << "Wrong number of stands to function run" << 81 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 81*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run82() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 82 ) {
        cerr << "Wrong number of stands to function run" << 82 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 82*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run83() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 83 ) {
        cerr << "Wrong number of stands to function run" << 83 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 83*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run84() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 84 ) {
        cerr << "Wrong number of stands to function run" << 84 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 84*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run85() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 85 ) {
        cerr << "Wrong number of stands to function run" << 85 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 85*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run86() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 86 ) {
        cerr << "Wrong number of stands to function run" << 86 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 86*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run87() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 87 ) {
        cerr << "Wrong number of stands to function run" << 87 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 87*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run88() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 88 ) {
        cerr << "Wrong number of stands to function run" << 88 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 88*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run89() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 89 ) {
        cerr << "Wrong number of stands to function run" << 89 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 89*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run90() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 90 ) {
        cerr << "Wrong number of stands to function run" << 90 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 90*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run91() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 91 ) {
        cerr << "Wrong number of stands to function run" << 91 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 91*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run92() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 92 ) {
        cerr << "Wrong number of stands to function run" << 92 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 92*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run93() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 93 ) {
        cerr << "Wrong number of stands to function run" << 93 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 93*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run94() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 94 ) {
        cerr << "Wrong number of stands to function run" << 94 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 94*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run95() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 95 ) {
        cerr << "Wrong number of stands to function run" << 95 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 95*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run96() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 96 ) {
        cerr << "Wrong number of stands to function run" << 96 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 96*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run97() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 97 ) {
        cerr << "Wrong number of stands to function run" << 97 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 97*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run98() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 98 ) {
        cerr << "Wrong number of stands to function run" << 98 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 98*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run99() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 99 ) {
        cerr << "Wrong number of stands to function run" << 99 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 99*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run100() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 100 ) {
        cerr << "Wrong number of stands to function run" << 100 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 100*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run101() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 101 ) {
        cerr << "Wrong number of stands to function run" << 101 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 101*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run102() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 102 ) {
        cerr << "Wrong number of stands to function run" << 102 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 102*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run103() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 103 ) {
        cerr << "Wrong number of stands to function run" << 103 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 103*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run104() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 104 ) {
        cerr << "Wrong number of stands to function run" << 104 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 104*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run105() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 105 ) {
        cerr << "Wrong number of stands to function run" << 105 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 105*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run106() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 106 ) {
        cerr << "Wrong number of stands to function run" << 106 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 106*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run107() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 107 ) {
        cerr << "Wrong number of stands to function run" << 107 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 107*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run108() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 108 ) {
        cerr << "Wrong number of stands to function run" << 108 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 108*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run109() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 109 ) {
        cerr << "Wrong number of stands to function run" << 109 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 109*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run110() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 110 ) {
        cerr << "Wrong number of stands to function run" << 110 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 110*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run111() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 111 ) {
        cerr << "Wrong number of stands to function run" << 111 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 111*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run112() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 112 ) {
        cerr << "Wrong number of stands to function run" << 112 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 112*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run113() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 113 ) {
        cerr << "Wrong number of stands to function run" << 113 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 113*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run114() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 114 ) {
        cerr << "Wrong number of stands to function run" << 114 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 114*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run115() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 115 ) {
        cerr << "Wrong number of stands to function run" << 115 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 115*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run116() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 116 ) {
        cerr << "Wrong number of stands to function run" << 116 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 116*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run117() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 117 ) {
        cerr << "Wrong number of stands to function run" << 117 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 117*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run118() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 118 ) {
        cerr << "Wrong number of stands to function run" << 118 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 118*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run119() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 119 ) {
        cerr << "Wrong number of stands to function run" << 119 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 119*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run120() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 120 ) {
        cerr << "Wrong number of stands to function run" << 120 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 120*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run121() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 121 ) {
        cerr << "Wrong number of stands to function run" << 121 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 121*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run122() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 122 ) {
        cerr << "Wrong number of stands to function run" << 122 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 122*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run123() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 123 ) {
        cerr << "Wrong number of stands to function run" << 123 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 123*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run124() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 124 ) {
        cerr << "Wrong number of stands to function run" << 124 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 124*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run125() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 125 ) {
        cerr << "Wrong number of stands to function run" << 125 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 125*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run126() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 126 ) {
        cerr << "Wrong number of stands to function run" << 126 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 126*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run127() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 127 ) {
        cerr << "Wrong number of stands to function run" << 127 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 127*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run128() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 128 ) {
        cerr << "Wrong number of stands to function run" << 128 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 128*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run129() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 129 ) {
        cerr << "Wrong number of stands to function run" << 129 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 129*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run130() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 130 ) {
        cerr << "Wrong number of stands to function run" << 130 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 130*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run131() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 131 ) {
        cerr << "Wrong number of stands to function run" << 131 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 131*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run132() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 132 ) {
        cerr << "Wrong number of stands to function run" << 132 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 132*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run133() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 133 ) {
        cerr << "Wrong number of stands to function run" << 133 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 133*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run134() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 134 ) {
        cerr << "Wrong number of stands to function run" << 134 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 134*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run135() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 135 ) {
        cerr << "Wrong number of stands to function run" << 135 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 135*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run136() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 136 ) {
        cerr << "Wrong number of stands to function run" << 136 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 136*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run137() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 137 ) {
        cerr << "Wrong number of stands to function run" << 137 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 137*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run138() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 138 ) {
        cerr << "Wrong number of stands to function run" << 138 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 138*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run139() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 139 ) {
        cerr << "Wrong number of stands to function run" << 139 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 139*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run140() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 140 ) {
        cerr << "Wrong number of stands to function run" << 140 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 140*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run141() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 141 ) {
        cerr << "Wrong number of stands to function run" << 141 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 141*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run142() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 142 ) {
        cerr << "Wrong number of stands to function run" << 142 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 142*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run143() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 143 ) {
        cerr << "Wrong number of stands to function run" << 143 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 143*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run144() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 144 ) {
        cerr << "Wrong number of stands to function run" << 144 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 144*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run145() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 145 ) {
        cerr << "Wrong number of stands to function run" << 145 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 145*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run146() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 146 ) {
        cerr << "Wrong number of stands to function run" << 146 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 146*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run147() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 147 ) {
        cerr << "Wrong number of stands to function run" << 147 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 147*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run148() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 148 ) {
        cerr << "Wrong number of stands to function run" << 148 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 148*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run149() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 149 ) {
        cerr << "Wrong number of stands to function run" << 149 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 149*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run150() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 150 ) {
        cerr << "Wrong number of stands to function run" << 150 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 150*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run151() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 151 ) {
        cerr << "Wrong number of stands to function run" << 151 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 151*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run152() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 152 ) {
        cerr << "Wrong number of stands to function run" << 152 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 152*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run153() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 153 ) {
        cerr << "Wrong number of stands to function run" << 153 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 153*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run154() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 154 ) {
        cerr << "Wrong number of stands to function run" << 154 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 154*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run155() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 155 ) {
        cerr << "Wrong number of stands to function run" << 155 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 155*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run156() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 156 ) {
        cerr << "Wrong number of stands to function run" << 156 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 156*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run157() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 157 ) {
        cerr << "Wrong number of stands to function run" << 157 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 157*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run158() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 158 ) {
        cerr << "Wrong number of stands to function run" << 158 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 158*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run159() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 159 ) {
        cerr << "Wrong number of stands to function run" << 159 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 159*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run160() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 160 ) {
        cerr << "Wrong number of stands to function run" << 160 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 160*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run161() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 161 ) {
        cerr << "Wrong number of stands to function run" << 161 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 161*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run162() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 162 ) {
        cerr << "Wrong number of stands to function run" << 162 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 162*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run163() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 163 ) {
        cerr << "Wrong number of stands to function run" << 163 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 163*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run164() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 164 ) {
        cerr << "Wrong number of stands to function run" << 164 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 164*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run165() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 165 ) {
        cerr << "Wrong number of stands to function run" << 165 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 165*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run166() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 166 ) {
        cerr << "Wrong number of stands to function run" << 166 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 166*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run167() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 167 ) {
        cerr << "Wrong number of stands to function run" << 167 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 167*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run168() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 168 ) {
        cerr << "Wrong number of stands to function run" << 168 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 168*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run169() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 169 ) {
        cerr << "Wrong number of stands to function run" << 169 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 169*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run170() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 170 ) {
        cerr << "Wrong number of stands to function run" << 170 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 170*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run171() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 171 ) {
        cerr << "Wrong number of stands to function run" << 171 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 171*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run172() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 172 ) {
        cerr << "Wrong number of stands to function run" << 172 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 172*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run173() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 173 ) {
        cerr << "Wrong number of stands to function run" << 173 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 173*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run174() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 174 ) {
        cerr << "Wrong number of stands to function run" << 174 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 174*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run175() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 175 ) {
        cerr << "Wrong number of stands to function run" << 175 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 175*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run176() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 176 ) {
        cerr << "Wrong number of stands to function run" << 176 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 176*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run177() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 177 ) {
        cerr << "Wrong number of stands to function run" << 177 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 177*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run178() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 178 ) {
        cerr << "Wrong number of stands to function run" << 178 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 178*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run179() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 179 ) {
        cerr << "Wrong number of stands to function run" << 179 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 179*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run180() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 180 ) {
        cerr << "Wrong number of stands to function run" << 180 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 180*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run181() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 181 ) {
        cerr << "Wrong number of stands to function run" << 181 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 181*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run182() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 182 ) {
        cerr << "Wrong number of stands to function run" << 182 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 182*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run183() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 183 ) {
        cerr << "Wrong number of stands to function run" << 183 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 183*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run184() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 184 ) {
        cerr << "Wrong number of stands to function run" << 184 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 184*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run185() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 185 ) {
        cerr << "Wrong number of stands to function run" << 185 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 185*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run186() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 186 ) {
        cerr << "Wrong number of stands to function run" << 186 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 186*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run187() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 187 ) {
        cerr << "Wrong number of stands to function run" << 187 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 187*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run188() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 188 ) {
        cerr << "Wrong number of stands to function run" << 188 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 188*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run189() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 189 ) {
        cerr << "Wrong number of stands to function run" << 189 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 189*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run190() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 190 ) {
        cerr << "Wrong number of stands to function run" << 190 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 190*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run191() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 191 ) {
        cerr << "Wrong number of stands to function run" << 191 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 191*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run192() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 192 ) {
        cerr << "Wrong number of stands to function run" << 192 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 192*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run193() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 193 ) {
        cerr << "Wrong number of stands to function run" << 193 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 193*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run194() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 194 ) {
        cerr << "Wrong number of stands to function run" << 194 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 194*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run195() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 195 ) {
        cerr << "Wrong number of stands to function run" << 195 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 195*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run196() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 196 ) {
        cerr << "Wrong number of stands to function run" << 196 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 196*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run197() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 197 ) {
        cerr << "Wrong number of stands to function run" << 197 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 197*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run198() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 198 ) {
        cerr << "Wrong number of stands to function run" << 198 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 198*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run199() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 199 ) {
        cerr << "Wrong number of stands to function run" << 199 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 199*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run200() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 200 ) {
        cerr << "Wrong number of stands to function run" << 200 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 200*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run201() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 201 ) {
        cerr << "Wrong number of stands to function run" << 201 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 201*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run202() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 202 ) {
        cerr << "Wrong number of stands to function run" << 202 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 202*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run203() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 203 ) {
        cerr << "Wrong number of stands to function run" << 203 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 203*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run204() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 204 ) {
        cerr << "Wrong number of stands to function run" << 204 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 204*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run205() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 205 ) {
        cerr << "Wrong number of stands to function run" << 205 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 205*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run206() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 206 ) {
        cerr << "Wrong number of stands to function run" << 206 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 206*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run207() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 207 ) {
        cerr << "Wrong number of stands to function run" << 207 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 207*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run208() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 208 ) {
        cerr << "Wrong number of stands to function run" << 208 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 208*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run209() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 209 ) {
        cerr << "Wrong number of stands to function run" << 209 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 209*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run210() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 210 ) {
        cerr << "Wrong number of stands to function run" << 210 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 210*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run211() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 211 ) {
        cerr << "Wrong number of stands to function run" << 211 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 211*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run212() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 212 ) {
        cerr << "Wrong number of stands to function run" << 212 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 212*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run213() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 213 ) {
        cerr << "Wrong number of stands to function run" << 213 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 213*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run214() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 214 ) {
        cerr << "Wrong number of stands to function run" << 214 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 214*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run215() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 215 ) {
        cerr << "Wrong number of stands to function run" << 215 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 215*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run216() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 216 ) {
        cerr << "Wrong number of stands to function run" << 216 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 216*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run217() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 217 ) {
        cerr << "Wrong number of stands to function run" << 217 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 217*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run218() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 218 ) {
        cerr << "Wrong number of stands to function run" << 218 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 218*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run219() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 219 ) {
        cerr << "Wrong number of stands to function run" << 219 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 219*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run220() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 220 ) {
        cerr << "Wrong number of stands to function run" << 220 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 220*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run221() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 221 ) {
        cerr << "Wrong number of stands to function run" << 221 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 221*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run222() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 222 ) {
        cerr << "Wrong number of stands to function run" << 222 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 222*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run223() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 223 ) {
        cerr << "Wrong number of stands to function run" << 223 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 223*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run224() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 224 ) {
        cerr << "Wrong number of stands to function run" << 224 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 224*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run225() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 225 ) {
        cerr << "Wrong number of stands to function run" << 225 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 225*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run226() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 226 ) {
        cerr << "Wrong number of stands to function run" << 226 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 226*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run227() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 227 ) {
        cerr << "Wrong number of stands to function run" << 227 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 227*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run228() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 228 ) {
        cerr << "Wrong number of stands to function run" << 228 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 228*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run229() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 229 ) {
        cerr << "Wrong number of stands to function run" << 229 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 229*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run230() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 230 ) {
        cerr << "Wrong number of stands to function run" << 230 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 230*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run231() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 231 ) {
        cerr << "Wrong number of stands to function run" << 231 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 231*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run232() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 232 ) {
        cerr << "Wrong number of stands to function run" << 232 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 232*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run233() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 233 ) {
        cerr << "Wrong number of stands to function run" << 233 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 233*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run234() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 234 ) {
        cerr << "Wrong number of stands to function run" << 234 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 234*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run235() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 235 ) {
        cerr << "Wrong number of stands to function run" << 235 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 235*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run236() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 236 ) {
        cerr << "Wrong number of stands to function run" << 236 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 236*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run237() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 237 ) {
        cerr << "Wrong number of stands to function run" << 237 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 237*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run238() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 238 ) {
        cerr << "Wrong number of stands to function run" << 238 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 238*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run239() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 239 ) {
        cerr << "Wrong number of stands to function run" << 239 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 239*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run240() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 240 ) {
        cerr << "Wrong number of stands to function run" << 240 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 240*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run241() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 241 ) {
        cerr << "Wrong number of stands to function run" << 241 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 241*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run242() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 242 ) {
        cerr << "Wrong number of stands to function run" << 242 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 242*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run243() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 243 ) {
        cerr << "Wrong number of stands to function run" << 243 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 243*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run244() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 244 ) {
        cerr << "Wrong number of stands to function run" << 244 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 244*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run245() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 245 ) {
        cerr << "Wrong number of stands to function run" << 245 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 245*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run246() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 246 ) {
        cerr << "Wrong number of stands to function run" << 246 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 246*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run247() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 247 ) {
        cerr << "Wrong number of stands to function run" << 247 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 247*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run248() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 248 ) {
        cerr << "Wrong number of stands to function run" << 248 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 248*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run249() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 249 ) {
        cerr << "Wrong number of stands to function run" << 249 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 249*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
void Optimize::run250() {
    // Flatten 
    double *flat_J;

    if ( stands.size() != 250 ) {
        cerr << "Wrong number of stands to function run" << 250 << endl;
        exit(1);
   }

    flat_J = new double[stands.size()*8];
    if ( flat_J == NULL ) {
      cerr  << "Failed to allocate flat_J in Optimize\n";
      exit(1);
    }

    for (int j=0; j<stands.size(); ++j) {
    	flat_J[j*8+0] = J[j].get(0, 0).real(); flat_J[j*8+1] = J[j].get(0, 0).imag();
    	flat_J[j*8+2] = J[j].get(0, 1).real(); flat_J[j*8+3] = J[j].get(0, 1).imag();
    	flat_J[j*8+4] = J[j].get(1, 0).real(); flat_J[j*8+5] = J[j].get(1, 0).imag();
    	flat_J[j*8+6] = J[j].get(1, 1).real(); flat_J[j*8+7] = J[j].get(1, 1).imag();
    }
	    
    Problem problem;
	for (int i=0; i<stands.size(); ++i) 
		for (int j=i+1; j<stands.size(); ++j) {
			problem.AddResidualBlock(
					new AutoDiffCostFunction<BaselineResidual, 8, 250*8>(new BaselineResidual(V[i][j], P[i][j], i, j)),
					NULL,
					flat_J);
     }
     
    //for (int i=0; i<stands.size(); ++i) J[i] = J[i]*0.5;
	
	Solver::Options options;
	options.max_num_iterations = 500;
	options.linear_solver_type = ceres::DENSE_QR;
	options.minimizer_progress_to_stdout = false;
        options.num_threads = stands.size()==3?1:32;

	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	for (int j=0; j<stands.size(); ++j) {
		J[j].set(complex<float>(flat_J[j*8+0], flat_J[j*8+1]), complex<float>(flat_J[j*8+2], flat_J[j*8+3]),
				complex<float>(flat_J[j*8+4], flat_J[j*8+5]), complex<float>(flat_J[j*8+6], flat_J[j*8+7]));
	}


    delete[] flat_J;
    

}
