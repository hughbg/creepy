/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <algorithm>
#include <thread>
#include <fstream>
#include "optimize.h"
#include "work.h"


int _hyperriggers[] = { 57, 58, 59, 60, 61, 62, 63, 64, 121, 122, 123, 124, 125, 126, 127, 128, 185, 186, 187, 188, 189, 190, 191, 192, 239, 240, 241, 242, 243, 244, 245, 246 };   // 1-based

static bool in(vector<int> v, int st) {
	for (int i=0; i<v.size(); ++i)
		if ( v[i] == st ) return true;
	return false;
}

static bool has_baseline_with(Baselines& bl, vector<int> current, int st) {
    
	for (int i=0; i<current.size(); ++i)
		if ( bl.find(current[i], st) != -1 && bl.baselines[bl.find(current[i], st)].has_data ) return true;
	return false;
}

class ThreadData {
public:
	vector<int> best_of;
	float best_res;
	vector<int> *list_of_3s;
	JonesMatrix<float> *J;
	Baselines* bl;
	thread *pthread;
	
	ThreadData(Baselines* _bl, JonesMatrix<float> *_J, vector<int> *_list_of_3s) { bl = _bl; J = _J; list_of_3s = _list_of_3s; }
};

void thread_optimize(ThreadData *data) {
	StandMapping stand_map;
	data->best_res = 1e39;

	for (int i=0; i<WORK_PER_THREAD && data->list_of_3s[i].size() != 0; ++i) {
		stand_map.clear();
		for (int j=0; j<data->list_of_3s[i].size(); ++j) stand_map.add(data->list_of_3s[i][j]);
		Optimize opt(stand_map, *data->bl, data->J);
		opt.run();
		float res = opt.residual();
		if ( res < data->best_res ) {
			data->best_res = res;
			data->best_of = stand_map.mapping;
		}
	}
}

void run_opt() {
	Baselines bl("vis.dat");
	StandMapping stands;
	JonesMatrix<float> *J;
	float f;
	
	if ( (J = new JonesMatrix<float>[256]) == NULL ) {
		cerr << "Failed to allocate J\n";
		return;
	}
	for (int i=0; i<256; ++i) J[i] = bl.init_jones[i];
	  
	vector<int> use_set;
	use_set.push_back(bl.stands_present[0]);	// assuming all bad stands flagged including hyperriggers
	for (int i=1; i<bl.stands_present.size(); ++i)
		if ( has_baseline_with(bl, use_set, i) ) use_set.push_back(i);
		else cout << "Stand " << i << " has no baselines connected to it\n";
	   
	cout << "Stands for optimizing ";
	for (int i=0; i<stands.num_mapped(); ++i) cout << stands.map(i) << " ";
	cout << endl;
	stands.add(use_set);
	Optimize opt(stands, bl, J);
	cout << "Optimizing\n";
	cout << opt.residual() << endl;
	opt.run();
	cout << opt.residual() << endl;
	
	
	for (int i=0; i<stands.num_mapped(); ++i) J[stands.map(i)] = opt.J[i];
	 	
	// Write Jones
    ofstream J_file("Jones.dat", ios::out | ios::binary);
    for (int i=0; i<256; ++i)
      for (int j=0; j<4; ++j) {
    	  f = J[i][j].real();
    	  J_file.write((const char*)&f, sizeof(float));	
    	  f = J[i][j].imag();
    	  J_file.write((const char*)&f, sizeof(float));	
      }
    	  
}

int main() {
  Baselines bl("vis.dat");
  StandMapping stands;
  JonesMatrix<float> *J;
  int st1, st2, st3;
  vector<int> best_of;
  float best_res=1e9, best_res_factor;
  vector<int> hyperriggers;
  Work work;
  
  for (int i=0; i<32; ++i) hyperriggers.push_back(_hyperriggers[i]-1);

  /* Measure */
  /*float norm_sum = 0;
  for (int i=0; i<bl.num_baselines; ++i) {
	  JonesMatrix j = bl.baselines[i].vis-bl.init_jones[bl.baselines[i].st1]*bl.baselines[i].model*bl.init_jones[bl.baselines[i].st2].conjugate_transpose();
	  norm_sum += j.norm()/bl.baselines[i].vis.norm();
  }
  cout << norm_sum/bl.num_baselines*1000 << endl;exit(0);*/
  

  run_opt(); return 0;


  vector<ThreadData*> threads;
  for (int i=0; i<bl.stands_present.size(); ++i) {
	  if ( i > 0 ) cout << "Done " << i << endl;
	  for (int j=0; j<bl.stands_present.size(); ++j) {
		  for (int k=0; k<bl.stands_present.size(); ++k) {
			  
			  stands.clear();
			  st1 = bl.stands_present[i]; st2 = bl.stands_present[j]; st3 = bl.stands_present[k];
			  if ( !in(hyperriggers, st1) && !in(hyperriggers, st2) && !in(hyperriggers, st3) && bl.find(st1, st2) > -1 && 
				bl.find(st1, st3) > -1 && bl.find(st2, st3) > -1 &&
				bl.baselines[bl.find(st1, st2)].has_data && bl.baselines[bl.find(st1, st3)].has_data && bl.baselines[bl.find(st2, st3)].has_data ) {
				  stands.add(st1); stands.add(st2); stands.add(st3); 

				  if ( work.full() ) {
					  // fire off threads
					  for (int it=0; it<MAX_THREADS; ++it) {
						  cout << "Start thread " << it << endl;
						  ThreadData *tdata = new ThreadData(&bl, J, &work.work[it][0]);
						  if ( tdata == NULL ) {
						    cerr << "Failed to allocate tdata\n";
						    return 1;
						  }
						  tdata->pthread = new thread(thread_optimize, tdata);
						  if ( tdata->pthread == NULL ) {
                                                    cerr << "Failed to allocate pthread\n";
                                                    return 1;
                                                  }	
						  threads.push_back(tdata);
					  }
					  
					  // wait for all
					  cout << "Waiting\n";
					  for (int it=0; it<threads.size(); ++it)  {						  
						  threads[it]->pthread->join();
						  cout << "Joined thread " << it << endl;
						  
						  if ( threads[it]->best_res < best_res ) {
							  best_res = threads[it]->best_res;
							  best_of = threads[it]->best_of;
							  cout << "New best ";
							  for (int b=0; b<best_of.size(); ++b) cout << best_of[b] << " ";
							  cout << endl;
							  cout << "Res " << best_res << " " << endl;
						  }
						  
						  
						  
						  delete threads[it]->pthread;
						  delete threads[it]; 
					  }
					  threads.clear(); 
					  work.clear();
					  
				  } else work.add(stands.mapping);
				        
				  		
			  }
				  
		  }
	  }  
	  
  }
  
  if ( threads.size() != 0 ) {
	  for (int it=0; it<threads.size(); ++it)  {						  
		  threads[it]->pthread->join();
		  cout << "Joined thread " << it << endl;
		  float res = threads[it]->best_res; 
		  if ( res < best_res ) {
			  best_res = res;
			  best_of = threads[it]->best_of;
			  stands.print(); cout << best_res <<endl;
		  }
		  
		  
		  delete threads[it]->pthread;
		  delete threads[it];
	  }
	  threads.clear(); 
 }

  
  cout << "Best of 3 interlinked: ";
  for (int i=0; i<best_of.size(); ++i) cout << best_of[i] << " ";
  cout << endl;
  cout << best_res << " " << endl;
 
  
  // Loop until best of contains all stands_present. 
  vector<int>  current_set = best_of;
  while ( current_set.size() < bl.stands_present.size() ) {
	  best_res=1e9;
	  for (int i=0; i<bl.stands_present.size(); ++i) {
		  if ( !in(hyperriggers, bl.stands_present[i]) && !in(current_set, bl.stands_present[i]) && has_baseline_with(bl, current_set, bl.stands_present[i]) ) {
			  stands.clear();
			  stands.add(current_set);
			  stands.add(bl.stands_present[i]);
			  sort(stands.mapping.begin(), stands.mapping.end()); 
			  //stands.print();
			  Optimize opt(stands, bl, J);
		  				  
			  float res1 = opt.residual();
			  opt.run();
			  float res2 = opt.residual(); 
			  
			  if ( res2 < best_res ) {
				  best_res = res2;
				  best_of = stands.mapping;
				  best_res_factor = res1/res2; 
			  }
		  }
	  }
	  for (int i=0; i<best_of.size(); ++i) cout << best_of[i] << " ";
	  cout << " | ";
	  cout << best_res << " " << best_res_factor << endl;
 
	  // Restore solved J into global J
	  current_set = best_of; 
	  stands.clear();
	  stands.add(current_set);
	  Optimize opt(stands, bl, J);
	  opt.run();
	  for (int i=0; i<stands.num_mapped(); ++i) J[stands.map(i)] = opt.J[i];
	  
  }
  

}
