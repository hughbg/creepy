/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <cstdlib>
#include "matrix.h"

template<typename T>
JonesMatrix<T>::JonesMatrix(const JonesMatrix<T>& jm) {
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; ++j) data[i][j] = jm.get(i, j);
}

template<typename T>
complex<T>& JonesMatrix<T>::operator[](int i) {       // indexing 0, 1, 2, 3
	switch(i) {
	case 0: return data[0][0]; break;
	case 1: return data[0][1]; break;
	case 2: return data[1][0]; break;
	case 3: return data[1][1]; break;
	default:
		cerr << "Invalid index " << i << " passed to JonesMatrix<T> get\n";
		exit(1);
	}
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::operator-(const JonesMatrix<T>& jm) {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; ++j) { 
			result.set(i, j, data[i][j]-jm.get(i, j));
		}
	return result;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::operator*(const JonesMatrix<T>& jm) {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			complex<T> c = complex<T>(T(0), T(0));
			
			for (int k=0; k<2; ++k) {   	
				c += data[i][k]*jm.get(k, j); 
			}
			result.set(i, j, c);
		}
	}
	return result;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::operator*(const T f) {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; ++j) result.set(i, j, data[i][j]*f);
	return result;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::operator/(const JonesMatrix<T>& jm) { 
	return (*this)*jm.inverse();
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::operator/(const T f) { 
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i) 
		for (int j=0; j<2; ++j) 	
			result.set(i, j, data[i][j]/f); 
	return result;	
}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator=(const JonesMatrix<T>& jm) {
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; ++j) data[i][j] = jm.get(i, j);
	return (*this);
}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator*=(const JonesMatrix<T>& jm) {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			for (int k=0; k<2; ++k) {   
				 result.set_inc(i, j, data[i][k]*jm.get(k, j)); 
			}
			
		}
	}	
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) data[i][j] = result.get(i, j);
	}
	
	return (*this);
}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator*=(const T f) {
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			data[i][j] *= f;
		}
	}
	return (*this);

}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator+=(const JonesMatrix<T>& jm) {
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			data[i][j] += jm.get(i, j);
		}
	}
	return *this;

}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator-=(const JonesMatrix<T>& jm) {
        for (int i=0; i<2; ++i) {
                for (int j=0; j<2; ++j) {
                        data[i][j] -= jm.get(i, j);
                }
        }
        return *this;

}


template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator/=(const T f) {
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			data[i][j] /= f;
		}
	}
	return *this;
}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::operator/=(const complex<T> c) {
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			data[i][j] /= c;
		}
	}
	return *this;
}

template<typename T>
JonesMatrix<T>& JonesMatrix<T>::scale_by_weight(const JonesMatrix<T>& jm) {
	for (int i=0; i<2; ++i) {
		for (int j=0; j<2; ++j) {
			data[i][j] /= abs(jm.get(i, j)); 
		}
	}
	return *this;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::conjugate_transpose() {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; j++ ) 
	      result.set(i, j, conj(data[j][i]));
	  
	return result;

}

template<typename T>
void JonesMatrix<T>::invert_sign() {
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; j++ ) 
	      data[i][j] = -data[i][j];
	  
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::inverse() const {
	JonesMatrix<T> result;
	result.set(data[1][1], -data[0][1], -data[1][0], data[0][0]);
	result /= determinant();
	return result;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::real() {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; j++ ) 
	      result.set(i, j, complex<T>(data[i][j].real(), 0));
	return result;
}

template<typename T>
JonesMatrix<T> JonesMatrix<T>::cabs() {
	JonesMatrix<T> result;
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; j++ ) 
	      result.set(i, j, complex<T>(abs(data[i][j]), 0));
	return result;
}

template<typename T>
bool JonesMatrix<T>::is_flagged() {
	return data[0][0].real() < 0 || data[0][1].real() < 0 || data[1][0].real() < 0 || data[1][1].real() < 0;
}

template<typename T>
void JonesMatrix<T>::set_zero() {
  complex<T> c = complex<T>(0, 0);
  set(c, c, c, c);
}

template<typename T>
void JonesMatrix<T>::flatten(T *flattened) const {
  	flattened[0] = data[0][0].real(); flattened[1] = data[0][0].imag();
   	flattened[2] = data[0][1].real(); flattened[3] = data[0][1].imag();
   	flattened[4] = data[1][0].real(); flattened[5] = data[1][0].imag();
   	flattened[6] = data[1][1].real(); flattened[7] = data[1][1].imag();
}

#include <iomanip>
template<typename T>
void JonesMatrix<T>::print() const {
  for (int i=0; i<2; ++i) {
    for (int j=0; j<2; ++j)
      cout << setprecision( 7 ) << data[i][j] << " ";
    cout << endl;
  } 
}

// Testing ------------------------------
/*
template<typename T>
bool close(complex<T> c1, complex<T> c2) {
	const T limit = 0.00001;
	return fabs(c1.real()-c2.real()) < limit && fabs(c1.imag()-c2.imag()) < limit;
}

template<typename T>
bool check_against(JonesMatrix<T> j1, JonesMatrix<T> j2, const char *title) {

	if ( !(close(j1.get(0, 0), j2.get(0, 0)) && close(j1.get(0, 1), j2.get(0, 1)) && close(j1.get(1, 0), j2.get(1, 0)) && close(j1.get(1, 1),j2.get(1, 1))) ) {
		cerr << title << " failed\n";
		exit(1);
	}
	
}

template<typename T>
bool check_against(complex<T> c1, complex<T> c2, const char *title) {
	if ( !close(c1, c2) ) {
		cerr << title << " failed\n";
		exit(1);
	}
}

template<typename T>
complex<T> cf(T x, T y) { return complex<T>(x, y); }


int main() {
	JonesMatrix<double> jm(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0));		// constructor

	
	
	check_against(jm, JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)), "constructor");
	
	jm.set_identity(); check_against(jm, JonesMatrix<double>(cf(1.0,0.0), cf(0.0,0.0), cf(0.0,0.0), cf(1.0,0.0)), "identity");
	
	JonesMatrix<double> jm1 = jm; check_against(jm1, jm, "copy construct");
	
	jm1 = JonesMatrix<double>(cf(1.0,2.0), cf(3.0,4.0), cf(5.0,6.0), cf(7.0,8.0)); check_against(jm1, JonesMatrix<double>(cf(1.0,2.0), cf(3.0,4.0), cf(5.0,6.0), cf(7.0,8.0)), "assignment");
	
	jm = jm1.real(); check_against(jm, JonesMatrix<double>(cf(1.0,0.0), cf(3.0,0.0), cf(5.0,0.0), cf(7.0,0.0)), "to real");
	
	jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,2.0), cf(3.0,3.0), cf(4.0,4.0));
	jm = jm1.inverse(); check_against(jm, JonesMatrix<double>(cf(-1.0,1.0), cf(0.5, -0.5), cf(0.75, -0.75), cf(-0.25,0.25)), "inverse or determinant1");
	
	jm1 = JonesMatrix<double>(cf(4.0,0.0), cf(7.0,0.0), cf(2.0,0.0), cf(6.0,0.0));
	jm = jm1.inverse(); check_against(jm, JonesMatrix<double>(cf(0.6, 0.0), cf(-0.7, 0.0), cf(-0.2, 0.0), cf(0.4,0.0)), "inverse or determinant2");
	
	jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,2.0), cf(3.0,3.0), cf(4.0,4.0)).conjugate_transpose();
	check_against(jm1, JonesMatrix<double>(cf(1.0,-1.0), cf(3.0,-3.0), cf(2.0,-2.0), cf(4.0,-4.0)), "conjugate transpose");

    
    jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)); 
    jm1 /= cf(2.0,2.0); check_against(jm1, JonesMatrix<double>(cf(0.5,0.0), cf(0.0,-1.0), cf(2.5,0.0), cf(2.0,0.0)), "divide by complex /=");
  
    jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)); 
    jm1 /= 2; check_against(jm1, JonesMatrix<double>(cf(0.5,0.5), cf(1.0,-1.0), cf(2.5,2.5), cf(2.0,2.0)), "divide by T /=");
    
    
    jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)); jm = JonesMatrix<double>(cf(2.0,2.0), cf(4.0,4.0), cf(2.0,2.0), cf(8.0,6.0));
    jm1 += jm; check_against(jm1, JonesMatrix<double>(cf(3.0,3.0), cf(6.0,2.0), cf(7.0,7.0), cf(12.0,10.0)), "+=");
    
    jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)); 
    jm1 *= 7; check_against(jm1, JonesMatrix<double>(cf(7.0,7.0), cf(14.0,-14.0), cf(35.0,35.0), cf(28.0,28.0)), "by T *=");
    
    jm1 = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0)); jm = JonesMatrix<double>(cf(2.0,2.0), cf(4.0,4.0), cf(2.0,2.0), cf(8.0,6.0));
    jm1 *= jm; check_against(jm1, JonesMatrix<double>(cf(8.0,4.0), cf(28.0,4.0), cf(0.0,36.0), cf(8.0,96.0)), "by Jones *=");

    jm1 = JonesMatrix<double>(cf(2.0,2.0), cf(4.0,4.0), cf(2.0,2.0), cf(8.0,6.0)); jm = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0));
    JonesMatrix<double> jm2 = jm1/jm;  check_against(jm2, JonesMatrix<double>(cf(-0.413793,1.03448), cf(0.482759,-0.206897), cf(-0.5,2.5), cf(0.5,-0.5)), "by Jones /");

    jm1 = JonesMatrix<double>(cf(2.0,2.0), cf(4.0,4.0), cf(2.0,2.0), cf(-8.0,6.0))*10;  check_against(jm1, JonesMatrix<double>(cf(20.0, 20.0), cf(40.0,40.0), cf(20.0,20.0), cf(-80.0,60.0)), "by T *");
    
    jm1 = JonesMatrix<double>(cf(2.0,2.0), cf(4.0,4.0), cf(2.0,2.0), cf(-8.0,6.0))-JonesMatrix<double>(cf(1.0,1.0), cf(2.0,-2.0), cf(5.0,5.0), cf(4.0,4.0));  
    check_against(jm1, JonesMatrix<double>(cf(1.0,1.0), cf(2.0,6.0), cf(-3.0,-3.0), cf(-12.0,2.0)), "by Jones -");
    
    jm = JonesMatrix<double>(cf(1.0,1.0), cf(2.0,6.0), cf(-3.0,-3.0), cf(-12.0,2.0));
    check_against(jm[0], cf(1.0,1.0), "operator[]"); check_against(jm[1], cf(2.0,6.0), "operator[]");check_against(jm[2], cf(-3.0,-3.0), "operator[]");check_against(jm[3], cf(-12.0,2.0), "operator[]");
    
}
*/