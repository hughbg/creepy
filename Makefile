creep:  main.o optimize.o stand_map.o baselines.o  work.o matrix.cc  
	g++ -g -o creep main.o optimize.o stand_map.o baselines.o work.o matrix.cc -lceres  -lcamd -lamd -lccolamd -lcolamd -lcholmod  -lcxsparse -fopenmp -lpthread -lgomp -lm -lglog -llapack -lblas 

main.o:	main.cc baselines.h stand_map.h work.h optimize.h
	g++ -c -std=gnu++11 main.cc

stand_map.o: stand_map.cc stand_map.h
	g++ -c stand_map.cc

baselines.o: baselines.cc baselines.h
	g++ -c baselines.cc

work.o:	work.h work.cc
	g++ -c work.cc

optimize.o: optimize_starter.cc run_template.txt generate optimize.h
	sh generate > optimize.cc
	g++ -c optimize.cc  -I/usr/include/eigen3 
