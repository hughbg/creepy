/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#ifndef MATRIX_H
#define MATRIX_H

#ifndef FALSE
#define FALSE 0
#endif

#include <complex>
using namespace std;

#define SQR(x) ((x)*(conj(x))).real()

template<typename T>
class JonesMatrix {
  complex<T> data[2][2];
  T flattened[8];

public:
  bool in_use;

  JonesMatrix() { data[0][0] = data[0][1] = data[1][0] = data[1][1] = complex<T>(T(0), T(0));  in_use = true; }
  JonesMatrix(complex<T> a, complex<T> b, complex<T> c, complex<T> d) { set(a, b, c, d); }
  JonesMatrix(const JonesMatrix&);     // copy constructor very important to have a constructor of this form
                        	// so that objects returned from a function on the stack are
                        	// copied properly. Also need a constructor for empty.
  complex<T>& operator[](int r);        // indexing 0, 1, 2, 3
  JonesMatrix operator-(const JonesMatrix&);
  JonesMatrix operator*(const JonesMatrix&);
  JonesMatrix operator*(const T);
  JonesMatrix operator/(const JonesMatrix&);
  JonesMatrix operator/(const T);
  JonesMatrix& operator=(const JonesMatrix&);
  JonesMatrix& operator*=(const JonesMatrix&);
  JonesMatrix& operator*=(const T);
  JonesMatrix& operator+=(const JonesMatrix&);
  JonesMatrix& operator-=(const JonesMatrix&);
  JonesMatrix& operator/=(const T);
  JonesMatrix& operator/=(const complex<T>);
  JonesMatrix& scale_by_weight(const JonesMatrix&);
 
  JonesMatrix conjugate_transpose();
  complex<T> get(int r, int c) const { return data[r][c]; }
  void set(complex<T> a, complex<T> b, complex<T> c, complex<T> d) { data[0][0] = a; data[0][1] = b; data[1][0] = c; data[1][1] = d; }
  void set(int i, int j, complex<T> c) { data[i][j] = c; }
  void set_inc(int i, int j, complex<T> c) { data[i][j] += c; }
  
  
  bool is_zero() { return abs(data[0][0]) == 0 && abs(data[0][1]) == 0 && abs(data[1][0]) == 0 && abs(data[1][1]) == 0; }
  complex<T> nu1_norm() { return data[0][0]*data[0][0]+data[0][1]*data[0][1]; }
  complex<T> nu2_norm() { return data[1][0]*data[1][0]+data[1][1]*data[1][1]; } 
  T norm() { return sqrt(SQR(data[0][0])+SQR(data[0][1])+SQR(data[1][0])+SQR(data[1][1])); }
  complex<T> determinant() const { return data[0][0]*data[1][1]-data[1][0]*data[0][1]; }
  void set_identity() { complex<T> z1 = complex<T>(1, 0); complex<T> z0 = complex<T>(0, 0); set(z1, z0, z0, z1); }
  void make_all_positive();
  void invert_sign();
  void set_zero();
  void flatten(T *flattened) const;
  JonesMatrix inverse() const;
  JonesMatrix real();
  JonesMatrix cabs();
  bool is_flagged();
  
  void print() const;
};    


#endif
