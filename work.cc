/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Creepy, a program for 
    implementing "creeping" calibration of radio interferometric data
    using optimization routines.

    Creepy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Creepy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Creepy.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <cstdlib>
#include "work.h"

void Work::add(vector<int> stands) {
  bool added = false;

  for (int i=0; i<MAX_THREADS && !added; ++i) {
	  for (int j=0; j<WORK_PER_THREAD && !added; ++j) {
  
		  if ( work[i][j].size() == 0 ) {
			  work[i][j] = stands;
			  added = true;
		  }
	  }
  }
  
  if ( !added ) {
	  cerr << "Failed to add a set of 3: should check for full\n";
	  exit(1);
  }
}

bool Work::full() {
	for (int i=0; i<MAX_THREADS; ++i) {
		for (int j=0; j<WORK_PER_THREAD; ++j) {
			if ( work[i][j].size() == 0 ) return false;
		}
	}
	return true;
}

